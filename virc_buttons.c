#include "virc_buttons.h"

static const char* TAG = "virc-buttons";
static const char* TASK = "virc-buttons-task";

static TaskHandle_t buttons_task_handle = NULL;

static int64_t buttons_timer;

static virc_button_t button_A = {VIRC_BUTTONS_A_PIN, 1, 0, 0, 0};
static virc_button_t button_B = {VIRC_BUTTONS_B_PIN, 1, 0, 0, 0};
static virc_button_t button_C = {VIRC_BUTTONS_C_PIN, 1, 0, 0, 0};

static virc_button_t *buttons[VIRC_BUTTONS_COUNT] = {&button_A, &button_B, &button_C};

static void
button_init(virc_button_t *b)
{
  gpio_set_direction(b->gpio_num, GPIO_MODE_INPUT);
  gpio_pullup_en(b->gpio_num);
  gpio_pulldown_en(b->gpio_num);
  b->last_level = gpio_get_level(b->gpio_num);
  b->last_time = virc_millis();
}

static void
button_poll(virc_button_t *b)
{
  uint8_t level = gpio_get_level(b->gpio_num);
  int64_t now = virc_millis();
  if (level != b->last_level
      && (now - b->last_time) > VIRC_BUTTONS_DEBOUNCE) {
    b->last_level = level;
    b->last_time = now;
    if (level) {
      b->is_released = true;
    } else {
      b->is_pressed = true;
    }
  }
}

static bool
button_pressed(virc_button_t *b)
{
  bool pressed = b->is_pressed;
  if (pressed) {
    b->is_pressed = false;
  }
  return pressed;
}

static bool
button_released
(virc_button_t *b)
{
  bool released = b->is_released;
  if (released) {
    b->is_released = false;
  }
  return released;
}


bool
virc_buttons_a_pressed
(void)
{
  return button_pressed(&button_A);
}

bool
virc_buttons_b_pressed
(void)
{
  return button_pressed(&button_B);
}

bool
virc_buttons_c_pressed
(void)
{
  return button_pressed(&button_C);
}

bool
virc_buttons_a_released
(void)
{
  return button_released(&button_A);
}

bool
virc_buttons_b_released
(void)
{
  return button_released(&button_B);
}

bool
virc_buttons_c_released
(void)
{
  return button_released(&button_C);
}


static void
buttons_init
(void)
{
  virc_timer_reset(&buttons_timer);
  for (uint8_t i = 0; i < VIRC_BUTTONS_COUNT; i++) {
    button_init(buttons[i]);
  }
}

static void
buttons_spin
(void)
{
  for (uint8_t i = 0; i < VIRC_BUTTONS_COUNT; i++) {
    button_poll(buttons[i]);
  }
  if (virc_timer_has(&buttons_timer, 5000)) {
    ESP_LOGD(TASK, "task watermark=%d", uxTaskGetStackHighWaterMark(NULL));
  }
}

static void
buttons_task
(void * pvParameters)
{
  buttons_init();
  for (;;) {
    buttons_spin();
  }
}

void
virc_buttons_run
(void)
{
  xTaskCreate(buttons_task, TASK, VIRC_BUTTONS_TASK_STACK_SIZE,
    NULL, tskIDLE_PRIORITY, &buttons_task_handle);
  configASSERT( buttons_task_handle );
}
