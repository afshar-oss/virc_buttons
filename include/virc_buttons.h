
#ifndef _VIRC_BUTTONS_H
#define _VIRC_BUTTONS_H

#include "driver/gpio.h"
#include "virc_core.h"

#define VIRC_BUTTONS_TASK_STACK_SIZE (8192)

#define VIRC_BUTTONS_A_PIN GPIO_NUM_39
#define VIRC_BUTTONS_B_PIN GPIO_NUM_38
#define VIRC_BUTTONS_C_PIN GPIO_NUM_37
#define VIRC_BUTTONS_COUNT (3)
#define VIRC_BUTTONS_DEBOUNCE (20)
#define VIRC_BUTTON_DEF(pin) 


typedef struct _virc_button {
  uint8_t gpio_num;
  bool last_level;
  uint32_t last_time;
  bool is_pressed;
  bool is_released;
} virc_button_t;


bool virc_buttons_a_pressed(void);
bool virc_buttons_b_pressed(void);
bool virc_buttons_c_pressed(void);
bool virc_buttons_a_released(void);
bool virc_buttons_b_released(void);
bool virc_buttons_c_released(void);


void virc_buttons_run(void);

#endif
